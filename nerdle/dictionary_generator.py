import copy

class DictionaryGenerator:

	class OperatorPositions:
		def __init__(self, possible_operators, equals_idx):
			self.__possible_operators = []
			for i in range(equals_idx-2):
				self.__possible_operators.append([None])
			for pos, op_list in enumerate(possible_operators[1:equals_idx-1]):
				self.__possible_operators[pos] += op_list
			self.__ret_val = [None] * equals_idx
			self.__done = False

		def __recalculate_ret_val(self):
			for pos, idx in enumerate(self.__op_indices):
				self.__ret_val[pos+1] = self.__possible_operators[pos][idx]

		def __increment_positions(self):
			# Increment to the next possible operator position
			for pos, op_idx in enumerate(self.__op_indices):

				# We can only increment this position to be an operator if no adjacent positions have operators.
				# We can assume that all positions lower than the current one are at idx = 0, meaning No Operator,
				# because we only get here if we didn't immediately set the previous position to be No Operator.
				# Therefore, we only need to check if the position above us is already No Operator.
				if (pos == (len(self.__op_indices) - 1)) or (self.__op_indices[pos+1] == 0):
					self.__op_indices[pos] += 1
					if self.__op_indices[pos] >= len(self.__possible_operators[pos]):
						self.__op_indices[pos] = 0
					else:
						break

			# We should only exit the for loop above normally if every single index was rolled back over to 0,
			# indicating we've gone through every possibility.
			else:
				self.__done = True

		def __iter__(self):
			self.__done = False
			self.__op_indices = [0] * len(self.__possible_operators)
			self.__recalculate_ret_val()

			# The first possibility is the one with no operators, which is not allowed, so we
			# increment past it before returning the iterator.
			self.__increment_positions()

			return self

		def __next__(self):
			if self.__done:
				raise StopIteration
			else:
				self.__recalculate_ret_val()
				self.__increment_positions()

			return self.__ret_val

	class NumberOptions:
		def __init__(self, possible_numbers, operators, equals_idx):
			self.__possible_numbers = copy.deepcopy(possible_numbers[0:equals_idx])
			self.__expression = [0] * equals_idx
			self.__num_indices = [0] * equals_idx
			self.__operators = operators
			self.__done = False

			# 0-leading numbers are not allowed, so we can remove the 0s as possibilities from right after operators if
			# the number is at least 2 digits long.
			for pos, num_list in enumerate(self.__possible_numbers):

				# This position is the beginning of a number if it's at the beginning of the expression or right after an operator
				if (pos == 0) or (self.__operators[pos-1] is not None):

					# The next character is another digit of the number
					if (pos < (equals_idx - 1)) and (self.__operators[pos+1] is None):
						num_list.remove('0')

					# The next character is not a number and the previous character was division
					# No division by 0 allowed!
					elif (pos > 1) and (self.__operators[pos-1] == '/'):
						num_list.remove('0')

		def __recalculate_expression(self):
			for pos in range(len(self.__expression)):
				if self.__operators[pos] is not None:
					self.__expression[pos] = self.__operators[pos]
				else:
					self.__expression[pos] = self.__possible_numbers[pos][self.__num_indices[pos]]

		def __iter__(self):
			self.__done = False
			self.__num_indices = [0] * len(self.__expression)
			self.__recalculate_expression()
			return self

		def __next__(self):
			if self.__done:
				raise StopIteration

			else:
				self.__recalculate_expression()

				# Increment to the next possible operator position
				for pos, num_idx in enumerate(self.__num_indices):

					# Only increment if no operator is here.
					if self.__operators[pos] is None:
						self.__num_indices[pos] += 1
						if self.__num_indices[pos] >= len(self.__possible_numbers[pos]):
							self.__num_indices[pos] = 0
						else:
							break

				# We should only exit the for loop above normally if every single index was rolled back over to 0,
				# indicating we've gone through every possibility.
				else:
					self.__done = True

			return "".join(self.__expression)

	def __init__(self):
		self.__possible_numbers = []
		for i in range(8):
			self.__possible_numbers.append(list(map(lambda x: str(x), range(0,10))))

		# An equation cannot start or end with an operator.
		# The rules stipulate a number, not expression, is to the right side of the equals sign.
		# It's impossible to produce a 4-digit number with 3 slots to the left, so we can rule out
		# position 3 or to the left for the equals sign.
		# If we put the equals sign as far to right as possible, the previous expression must end in
		# a number, so we can rule out operators in position 5 or to the right.
		self.__possible_operators = [[]] + [['+', '-', '*', '/']] * 4 + [[]] * 3
		self.__possible_equals_indices = [4, 5, 6]

		self.__possible_eqs = []

	def __iter__(self):
		self.__init__()
		self.__equals_idx_iter = iter(self.__possible_equals_indices)
		self.__equals_idx = next(self.__equals_idx_iter)
		self.__operator_positions_iter = iter(self.OperatorPositions(self.__possible_operators, self.__equals_idx))
		operator_positions = next(self.__operator_positions_iter)
		self.__expr_iter = iter(self.NumberOptions(self.__possible_numbers, operator_positions, self.__equals_idx))
		return self

	def __next__(self):
		eq = None

		while eq is None:
			try:
				# Get next expression
				expr = next(self.__expr_iter)

			# If we've exhausted all expressions from this operation position
			except StopIteration:

				try:
					# Get next operator position
					operator_positions = next(self.__operator_positions_iter)

				# If we've exhausted all operator positions from this equals index
				except StopIteration:

					# Get next equals index. If this raises a StopIteration, it means our overall iteration is done is well,
					# so we don't try to rescue it.
					self.__equals_idx = next(self.__equals_idx_iter)

					# Get a new Operator Positions iterator from the new equals index
					self.__operator_positions_iter = iter(self.OperatorPositions(self.__possible_operators, self.__equals_idx))

					# Get the first position from the new iterator
					operator_positions = next(self.__operator_positions_iter)

				# Get a new Number Options iterator from the new operator position
				self.__expr_iter = iter(self.NumberOptions(self.__possible_numbers, operator_positions, self.__equals_idx))

				# Get the first expression from the new iterator
				expr = next(self.__expr_iter)

			# Check if the expression is valid
			result = eval(expr)
			int_result = int(result)
			if (float(int_result) == result) and (int_result >= 0):
				str_result = str(int_result)

				if len(str_result) == len(self.__possible_numbers) - self.__equals_idx - 1:
					eq = expr + '=' + str_result

		return eq
