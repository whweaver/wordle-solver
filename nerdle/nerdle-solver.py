#!/usr/bin/python

import sys
from nerdle import Nerdle

hard = False
num_nerdles = 1
if len(sys.argv) == 2:
	if sys.argv[1] == 'hard':
		hard = True
		num_nerdles = 1
	else:
		num_nerdles = int(sys.argv[1])
		if num_nerdles <= 0:
			raise f"Invalid number of nerdles: {sys.argv[1]}"
elif len(sys.argv) > 2:
	raise "Too many arguments!"

print("Welcome to the Nerdle solver!")
print("At each step, input Nerdle's response using the following rules:")
print("1) For characters not present, input '_'")
print("2) For characters present but in the wrong location, input the character 'y'")
print("3) For characters in the correct location, input the character 'g'")
print("An entire response should have 8 characters.")
print("Enter 'exit' to exit at any time.")

nerdles = []
for i in range(num_nerdles):
	nerdles.append({
		"game": Nerdle(hard),
		"solution": None,
		"solved": False
	})

while True:

	max_poss_equations = 0
	best_guesses = {}
	for nerdle in nerdles:
		poss_equations = nerdle["game"].possible_equations()
		if len(poss_equations) > max_poss_equations:
			max_poss_equations = len(poss_equations)
		print(f"There are {len(poss_equations)} possible equations.")
		if len(poss_equations) <= 10:
			print("\n".join(poss_equations))

		if len(poss_equations) == 1:
			nerdle["solution"] = poss_equations[0]
		else:
			bg = nerdle["game"].best_guesses(True)
			for equation, score in bg:
				if equation in best_guesses:
					if score > best_guesses[equation]:
						best_guesses[equation] = score
				else:
					best_guesses[equation] = score

	print(f"The top ten guessing equations are:")
	top_ten_best_guesses = []
	for nerdle in nerdles:
		if nerdle["solution"] and not nerdle["solved"]:
			top_ten_best_guesses.append(nerdle["solution"])
	sorted_best_guesses = sorted(best_guesses.items(), key=lambda x: x[1])
	top_ten_best_guesses += list(map(lambda x: x[0], sorted_best_guesses))
	print(" ".join(top_ten_best_guesses[0:10]))

	if max_poss_equations <= 1:
		exit()

	while True:
		entered_equation = input("Entered equation: ").upper()
		if entered_equation.lower() == "exit":
			exit()
		if len(entered_equation) != 8:
			print("Entered equation must be 8 characters.")
		elif ('_' in entered_equation) or ('y' in entered_equation) or ('g' in entered_equation):
			print("It looks like this is a response, not a equation. Please enter the equation you entered:")
		else:
			break

	for i, nerdle in enumerate(nerdles):
		if entered_equation == nerdle["solution"]:
			nerdle["solved"] = True

		if nerdle["solution"] is None:
			while True:
				response = input(f"Response {i+1}: ")
				if response.lower() == "exit":
					exit()
				if len(response) != 8:
					print("Response must be 8 characters.")
					continue
				for pos, char in enumerate(response):
					if (char.upper() != entered_equation[pos]) and (char != '_'):
						print("It looks like this is not a valid response for the equation you entered. Please enter the correct response:")
						break
				else:
					break

			nerdle["game"].guess(entered_equation, response)
