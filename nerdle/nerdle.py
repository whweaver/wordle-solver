from multiprocessing import Lock
import os
import json
import math
from datetime import datetime

from dictionary_generator import DictionaryGenerator

class Nerdle:

	__DICTIONARY_FILE = 'dictionary.json'
	__COMPARISON_FILE = 'comparisons.json'
	__PARTITIONS_FILE = 'partitions.json'

	__dict_cache = None
	__dict_lock = Lock()

	__part_cache = None
	__part_lock = Lock()

	def __init__(self, hard=False):
		self.__HARD = hard
		self.__char_counts = {}
		self.__possible_chars = []
		self.possible_chars = [list(range(0,10)),
							   list(range(0,10)) + ['+', '-', '*', '/'],
							   list(range(0,10)) + ['+', '-', '*', '/'],
							   list(range(0,10)) + ['+', '-', '*', '/'],
							   list(range(0,10)) + ['+', '-', '*', '/', '='],
							   list(range(0,10)) + ['='],
							   list(range(0,10)) + ['='],
							   list(range(0,10))]
		self.__possible_eqs = self.dictionary().copy()
		self.__guess_eq_scores = self.__partitions()

	def dictionary(self):
		with Nerdle.__dict_lock:
			if Nerdle.__dict_cache is not None:
				pass

			elif os.path.isfile(Nerdle.__DICTIONARY_FILE):
				print("Loading dictionary...")
				with open(Nerdle.__DICTIONARY_FILE) as f:
					Nerdle.__dict_cache = json.load(f)

			else:
				print("Computing dictionary...")
				Nerdle.__dict_cache = {}
				for idx, eq in enumerate(DictionaryGenerator()):
					Nerdle.__dict_cache[eq] = idx

				with open(Nerdle.__DICTIONARY_FILE, 'w') as f:
					json.dump(Nerdle.__dict_cache, f)

		return Nerdle.__dict_cache

	def __guess(self, guess_eq, actual_eq):
		comparison = ['_'] * 8

		# Find how many of each char there are in the actual equation
		available_char_counts = {}
		for char in actual_eq:
			if char not in available_char_counts:
				available_char_counts[char] = 0
			available_char_counts[char] += 1

		# Check for exact matches first
		# Doing this first ensures yellows don't appear for letters that are marked green elsewhere
		for pos, char in enumerate(guess_eq):
			if actual_eq[pos] == char:
				comparison[pos] = 'G'
				available_char_counts[char] -= 1

		# Check for other letters that are present but in the wrong spot
		for pos, char in enumerate(guess_eq):
			# Only mark a char as yellow if it is not already green and if there is more of that
			# char elsewhere in the equation
			if (comparison[pos] != 'G') and (char in available_char_counts) and (available_char_counts[char] > 0):
				comparison[pos] = 'y'
				available_char_counts[char] -= 1

		return "".join(comparison)

	def __preprocess_eq(self, guess_eq):
		lookup_table = []
		for actual_eq in self.dictionary():
			lookup_table.append(self.__guess(guess_eq, actual_eq))
		return lookup_table

	def __partition_score(self, eq):
		dictionary = self.dictionary()
		partition = {}
		num_partitions = 0
		for poss_eq in self.__possible_eqs:
			guess_idx = dictionary[eq]
			poss_idx = dictionary[poss_eq]
			comparison = self.__guess(eq, poss_eq)
			if comparison not in partition:
				partition[comparison] = [poss_eq]
				num_partitions += 1
			else:
				partition[comparison].append(poss_eq)

		LOG_DIVISOR = math.log(num_partitions)
		if LOG_DIVISOR == 0:
			return len(self.__possible_eqs) + 1

		CORRECT_COMPARISON = 'G'*8
		expected_moves = 0

		# Remove the correct comparison because its expected move is 0
		if CORRECT_COMPARISON in partition:
			partition.pop(CORRECT_COMPARISON)

		for comparison in partition:
			frac_of_total_space = len(partition[comparison]) / len(self.__possible_eqs)
			comparison_exp_moves = (math.log(len(partition[comparison])) / LOG_DIVISOR) + 1
			expected_moves += frac_of_total_space * comparison_exp_moves

		return expected_moves

	def __calculate_eq_scores(self):
		# Determine the best guess equation
		# This is done by choosing the equation whose possible results partition the remaining possible
		# equation list into the most equal chunks.
		guess_eq_scores = {}
		if self.__HARD:
			guessable_eqs = self.__possible_eqs
		else:
			guessable_eqs = self.dictionary()

		for guess_eq in guessable_eqs:
			print(guess_eq)
			guess_eq_scores[guess_eq] = self.__partition_score(guess_eq)

		return guess_eq_scores

	def __partitions(self):
		with Nerdle.__part_lock:
			if Nerdle.__part_cache is not None:
				pass

			elif os.path.isfile(Nerdle.__PARTITIONS_FILE):
				print("Loading partitions...")
				with open(Nerdle.__PARTITIONS_FILE) as f:
					Nerdle.__part_cache = json.load(f)

			else:
				print("Computing partitions...")
				Nerdle.__part_cache = self.__calculate_eq_scores()

				with open(self.PARTITION_FILE, 'w') as f:
					json.dump(Nerdle.__part_cache, f)

		return Nerdle.__part_cache

	def __parse_response_green_yellow(self, entered_eq, response):
		new_char_counts = {}

		response = response.upper()

		for pos, char in enumerate(response):
			if char == '_':
				# We'll deal with these later
				continue

			elif char == 'G':

				# Set position to be only this green char
				self.__possible_chars[pos] = [char]

				# Add char to must-haves
				if char not in new_char_counts:
					new_char_counts[char] = 1
				else:
					new_char_counts[char] += 1

			elif char == 'Y':

				# Add char to must-haves
				if char not in new_char_counts:
					new_char_counts[char] = 1
				else:
					new_char_counts[char] += 1

				# Remove char from this position
				if char in self.__possible_chars[pos]:
					self.__possible_chars[pos].remove(char)

			else:
				raise f"Unknown response character: {char}"

		# Check to see if we need more of a given char than we previously thought
		for char in new_char_counts:
			if char not in self.__char_counts:
				self.__char_counts[char] = [new_char_counts[char], None]
			elif new_char_counts[char] > self.__char_counts[char][0]:
				self.__char_counts[char][0] = new_char_counts[char]

	def __parse_response_gray(self, entered_eq, response):
		for pos, char in enumerate(response):
			if char == '_':
				char = entered_eq[pos]

				# Remove char from this position
				if char in self.__possible_chars[pos]:
					self.__possible_chars[pos].remove(char)

				# Set the maximum number of char occurrences if this char is a must-have
				if char in self.__char_counts:
					self.__char_counts[char][1] = self.__char_counts[char][0]

				# Remove char from other positions if it is not green or yellow elsewhere
				else:
					for pos_list in self.__possible_chars:
						if char in pos_list:
							pos_list.remove(char)

	def __pare_eq_list(self):
		new_possible_eqs = []
		for eq in self.__possible_eqs:

			# Check that all must-haves are in eq
			for char in self.__char_counts:
				if  (
						(self.__char_counts[char][0] is not None) and
						(eq.count(char) < self.__char_counts[char][0])
					) or (
						(self.__char_counts[char][1] is not None) and
				   		(eq.count(char) > self.__char_counts[char][1])
				   	):
				   break
			else:
				# Check that each position only has allowable letters for that position
				for pos, char in enumerate(eq):
					if char not in self.__possible_chars[pos]:
						break
				else:
					new_possible_eqs.append(eq)

		self.__possible_eqs = new_possible_eqs

	def guess(self, entered_eq, response):
		if len(entered_eq) != 8:
			raise "Entered equation is not 8 characters!"
		if len(response) != 8:
			raise "Response is not 8 characters!"

		# First, look for green and yellow characters
		self.__parse_response_green_yellow(entered_eq, response)

		# Next, look for gray characters
		# This is done in a separate iteration because if a character is repeated with
		# the first instance gray and the second yellow or green, we need to know
		# about that later instance before we can decide what to do with the gray
		# one.
		self.__parse_response_gray(entered_eq, response)

		# Pare down equation list from parsed response
		self.__pare_eq_list()

		# Calculate new best equation scores
		self.__guess_eq_scores = self.__calculate_eq_scores()

	def possible_equations(self):
		return self.__possible_eqs

	def best_guesses(self, with_scores=False):
		sorted_eq_scores = sorted(self.__guess_eq_scores.items(), key=lambda x: x[1])

		if with_scores:
			return sorted_eq_scores
		else:
			return list(map(lambda x: x[0], sorted_eq_scores))
