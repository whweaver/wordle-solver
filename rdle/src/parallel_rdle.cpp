#include "parallel_rdle.h"
#include <algorithm>
#include <string>
#include <vector>
#include <unordered_map>
#include <iostream>
#include "rdle.h"

/**
 * Compare the scores of two words.
 * 
 * @param a The first word/score pair.
 * @param b The second word/score pair.
 * 
 * @return True if a has a lower score than b, false otherwise.
 */
static bool compare_scores(Rdle::word_score_t& a, Rdle::word_score_t& b)
{
	return a.second < b.second;
}

/**
 * Convert a string to lowercase.
 * 
 * @param str The string to convert.
 * 
 * @return The lowercase string.
 */
static std::string str_to_lower(std::string& str)
{
	std::string lower = "";

	for(char c : str)
	{
		lower += std::tolower(c);
	}

	return lower;
}

/**
 * Convert a string to uppercase.
 * 
 * @param str The string to convert.
 * 
 * @return The uppercase string.
 */
static std::string str_to_upper(std::string& str)
{
	std::string upper = "";

	for(char c : str)
	{
		upper += std::toupper(c);
	}

	return upper;
}

/**
 * Delegated constructor.
 * 
 * @param num_rdles The number of rdles to play at once.
 * @param dict_file_path The file path to the dictionary.
 * @param comparisons_file_path The file path to the comparisons cache.
 * @param scores_file_path The file path to the scores cache.
 * @param hard Whether or not to use hard mode. This only works if one game is being played.
 */
void ParallelRdle::init(unsigned int num_rdles, std::string dict_file_path, std::string comparisons_file_path, std::string scores_file_path, bool hard)
{
	this->dict_file_path = dict_file_path;
	this->comparisons_file_path = comparisons_file_path;
	this->scores_file_path = scores_file_path;
	this->hard = hard;

	if(hard && (num_rdles > 1u))
	{
		throw;
	}
	else
	{
		this->games.clear();
		for(unsigned int i = 0u; i < num_rdles; i++)
		{
			this->games.emplace_back(dict_file_path, comparisons_file_path, scores_file_path, hard);
		}
	}

	this->responses = std::vector<std::string>(num_rdles, "");

	this->state = STATE_PRINT_STATUS;
}

ParallelRdle::ParallelRdle(unsigned int num_rdles, std::string dict_file_path, std::string comparisons_file_path, std::string scores_file_path, bool hard)
{
	this->init(num_rdles, dict_file_path, comparisons_file_path, scores_file_path, hard);
}

ParallelRdle::ParallelRdle(unsigned int num_rdles, std::string cache_dir_path, bool hard)
{
	this->init(num_rdles, cache_dir_path + "/dictionary.txt", cache_dir_path + "/comparisons.bin", cache_dir_path + "/scores.bin", hard);
}

ParallelRdle::word_list_t ParallelRdle::get_best_guesses(void)
{
	// The strategy here is to rank the guesses based on their
	// worst score: that is, find each possible guess word's worst
	// score among the active games, then choose the word with the
	// best worst score.
	std::unordered_map<std::string, double> guesses_worst_score;
	std::vector<std::string> solutions_to_guess;
	for(auto& rdle : this->games)
	{
		// If we know the solution to this game already, then we don't need to
		// compute the best guesses for it.
		std::string solution = rdle.get_solution();
		if(solution.size() > 0)
		{
			// If the game hasn't been solved yet, ensure that the solution is
			// at the front of the guess list.
			if(!rdle.is_solved())
			{
				solutions_to_guess.push_back(solution);
			}
			else
			{
				// This game has a known solution and the user has entered that solution, so
				// it's solved and shouldn't influence guesses anymore.
			}
		}
		else
		{
			Rdle::sorted_word_scores_t bg = rdle.get_best_guesses();
			for(auto& [word, score] : bg)
			{
				if(guesses_worst_score.count(word) > 0)
				{
					if(score > guesses_worst_score[word])
					{
						guesses_worst_score[word] = score;
					}
				}
				else
				{
					guesses_worst_score[word] = score;
				}
			}
		}
	}

	// Sort solutions by which one gives us the most information on the
	// remaining games. Also remove the solutions from the score list
	// since they're guaranteed to be at the beginning of the guess list
	// anyway.
	std::vector<std::pair<std::string, double>> sortable_solutions;
	for(auto& word : solutions_to_guess)
	{
		sortable_solutions.push_back(std::make_pair(word, guesses_worst_score[word]));
		guesses_worst_score.erase(word);
	}
	std::sort(sortable_solutions.begin(), sortable_solutions.end(), compare_scores);

	// Sort the remaining guess words according to their worst scores.
	std::vector<std::pair<std::string, double>> sortable_best_guesses;
	for(auto& [word, score] : guesses_worst_score)
	{
		sortable_best_guesses.push_back(std::make_pair(word, score));
	}
	std::sort(sortable_best_guesses.begin(), sortable_best_guesses.end(), compare_scores);

	std::vector<std::string> best_guesses;

	// Add the solutions to the front of the guess list
	for(auto& [word, score] : sortable_solutions)
	{
		best_guesses.push_back(word);
	}

	// Add the remaining guess words to the guess list
	for(auto& [word, score] : sortable_best_guesses)
	{
		best_guesses.push_back(word);
	}

	return best_guesses;
}

std::vector<Rdle::word_set_t> ParallelRdle::get_possible_words(void)
{
	std::vector<Rdle::word_set_t> poss_words;

	for(auto& rdle : this->games)
	{
		poss_words.push_back(rdle.get_possible_words());
	}

	return poss_words;
}

/**
 * Parse command and record it to be executed next cycle.
 * 
 * @param cmd The command to parse.
 * 
 * @return True if the given string was a command, false otherwise.
 */
bool ParallelRdle::parse_command(std::string cmd)
{
	cmd = str_to_lower(cmd);
	
	if( (cmd == "exit") ||
		(cmd.rfind(std::string("restart"), 0) == 0) ||
		(cmd == "undo"))
	{
		if(this->pending_command.size() > 0)
		{
			// Cannot parse a new command when one is already pending
			throw;
		}
		else
		{
			this->pending_command = cmd;
			return true;
		}
	}

	return false;
}

/**
 * Undo the last action
 */
void ParallelRdle::undo(void)
{
	if(this->state == STATE_RESPOND)
	{
		// Find the last recorded response and discard it
		for(int game_idx = this->responses.size() - 1u; game_idx > 0; game_idx--)
		{
			if(this->responses[game_idx].size() > 0u)
			{
				this->responses[game_idx] = "";
				return;
			}
		}

		// If we get here, there were no recorded responses yet, so the guess
		// should be discarded instead.
		this->guess_word = "";
		this->state = STATE_GUESS;
	}
	else
	{
		// Find the largest guess stack
		unsigned int most_guesses = 0u;
		for(auto& game : this->games)
		{
			if(game.num_guesses() > most_guesses)
			{
				most_guesses = game.num_guesses();
			}
		}

		// Undo the last guess for each game with the largest guess stack
		for(auto& game : this->games)
		{
			if(game.num_guesses() >= most_guesses)
			{
				game.undo();
			}
		}

		this->state = STATE_PRINT_STATUS;
	}
}

/**
 * Execute the pending command.
 */
void ParallelRdle::execute_command(void)
{
	if(this->pending_command == "exit")
	{
		this->state = STATE_EXIT;
	}
	else if(this->pending_command == "restart")
	{
		this->init(this->games.size(), this->dict_file_path, this->comparisons_file_path, this->scores_file_path, this->hard);
	}
	else if(this->pending_command.rfind(std::string("restart"), 0) == 0)
	{
		static const int num_offset = std::string("restart ").size();
		std::string num_str = this->pending_command.substr(num_offset);
		int num_rdles = stoi(num_str);
		if(num_rdles > 0)
		{
			this->init(num_rdles, this->dict_file_path, this->comparisons_file_path, this->scores_file_path, this->hard);
		}
		else
		{
			// Need a positive nonzero number of games
			throw;
		}
	}
	else if(this->pending_command == "undo")
	{
		this->undo();
	}
	else
	{
		// Invalid command
		throw;
	}

	this->pending_command = "";
}

/**
 * Get the guessed word from the command line interface.
 */
void ParallelRdle::enter_guess(void)
{
	while(true)
	{
		std::string guess = "";
		std::cout << "Guessed word: ";
		std::getline(std::cin, guess);
		if(this->parse_command(guess))
		{
			return;
		}
		else
		{
			Rdle::word_length_t word_len = this->games[0].get_word_len();
			if(guess.size() != word_len)
			{
				std::cout << "Guess must be " << (int)word_len << " characters.\n";
			}
			else
			{
				this->guess_word = str_to_upper(guess);
				this->responses = std::vector<std::string>(this->games.size(), "");
				this->state = STATE_RESPOND;
				break;
			}
		}
	}
}

/**
 * Get the response from the command line interface.
 */
void ParallelRdle::enter_response(void)
{
	unsigned int game_idx = 0u;

	// Find the next game to get the response for
	for(game_idx = 0u; game_idx < this->games.size(); game_idx++)
	{
		// Find the first game that doesn't have a response recorded yet
		// and also doesn't have a known solution yet. Note that this does
		// express a lot of confidence that our dictionary is accurate to
		// what the source game actually uses, since it would be nice to know
		// if future evidence ends up marking our solution as invalid.
		if( (this->responses[game_idx].size() == 0u) &&
			(this->games[game_idx].get_solution().size() == 0u))
		{
			// Break so we go get the response for this game.
			break;
		}
	}

	// Get the response for this game from the user.
	// Because game_idx doesn't change within this loop, this while
	// statement functions as a nested if() and while(true).
	while(game_idx < this->games.size())
	{
		std::string response = "";
		std::cout << "Response " << game_idx+1u << ": ";
		std::getline(std::cin, response);
		if(this->parse_command(response))
		{
			return;
		}
		else
		{
			response = str_to_upper(response);
			Rdle::word_length_t word_len = this->games.begin()->get_word_len();
			if(response.size() != word_len)
			{
				std::cout << "Response must be " << word_len << " characters.\n";
			}
			else
			{
				bool valid = true;
				for(char c : response)
				{
					if(!((c == '_') || (c == 'G') || (c == 'Y')))
					{
						valid = false;
						break;
					}
				}

				if(valid)
				{
					this->responses[game_idx] = response;
					break;
				}
				else
				{
					std::cout << "Response must contain only _, Y, or G.\n";
				}
			}
		}
	}

	if(game_idx >= (this->games.size() - 1u))
	{
		this->state = STATE_PROCESS_GUESSES;
	}
}

/**
 * Process guesses.
 */
void ParallelRdle::process_guesses(void)
{
	for(unsigned int game_idx = 0u; game_idx < this->games.size(); game_idx++)
	{
		if(this->responses[game_idx].size() > 0u)
		{
			// A response was gathered for this game
			this->games[game_idx].guess(this->guess_word, this->responses[game_idx]);
		}
		else if(this->guess_word == this->games[game_idx].get_solution())
		{
			// The solution to this game was entered (no response was given because the solution was already known)
			this->games[game_idx].guess(this->guess_word, std::string(this->games[game_idx].get_word_len(), 'G'));
		}
		else
		{
			// No response was given and no solution is known.
		}
	}

	this->guess_word = "";
	this->responses = std::vector<std::string>(this->games.size(), "");
	this->state = STATE_PRINT_STATUS;
}

/**
 * Print the possible words.
 */
void ParallelRdle::print_status(void)
{
	// Print possible words
	std::vector<Rdle::word_set_t> poss_words = this->get_possible_words();
	for(auto& poss_words_set : poss_words)
	{
		std::cout << "There are " << poss_words_set.size() << " possible_words.\n";
		if(poss_words_set.size() < 10u)
		{
			for(auto& word : poss_words_set)
			{
				std::cout << word << " ";
			}
			std::cout << std::endl;
		}
	}

	// Print best guessing words
	std::cout << "The top ten guessing words are:\n";
	Rdle::word_list_t best_guesses = this->get_best_guesses();
	Rdle::dict_length_t num_guesses = best_guesses.size() > 10u ? 10u : best_guesses.size();
	for(uint8_t i = 0u; i < num_guesses; i++)
	{
		std::cout << best_guesses[i] << " ";
	}
	std::cout << std::endl;

	// Go to next state
	this->state = STATE_GUESS;
}

/**
 * Process one cycle of the ParallelRdle game.
 */
void ParallelRdle::process(void)
{
	if(this->pending_command.size() > 0)
	{
		this->execute_command();
	}
	else
	{
		switch(this->state)
		{
			case STATE_PRINT_STATUS:
				this->print_status();
				break;

			case STATE_GUESS:
				this->enter_guess();
				break;

			case STATE_RESPOND:
				this->enter_response();
				break;

			case STATE_PROCESS_GUESSES:
				this->process_guesses();
				break;

			default:
				throw;
				break;
		}
	}
}

void ParallelRdle::run_cli(void)
{
	std::cout << "Welcome to the Wordle solver!\n";
	std::cout << "At each step, input Wordle's response using the following rules:\n";
	std::cout << "    For letters not present, input '_'\n";
	std::cout << "    For letters present but in the wrong location, input 'Y' or 'y'\n";
	std::cout << "    For letters in the correct location, input 'g' or 'G'\n";
	std::cout << "An entire response should have 5 characters. For example, if your word was SONAR, your\n";
	std::cout << "response might be __nA_, if the n was yellow, the A was green, and the other letters were\n";
	std::cout << "gray.\n";
	std::cout << "\n";
	std::cout << "You may enter special commands at any time as well:\n";
	std::cout << "    exit: Exit the program.\n";
	std::cout << "    restart: Restart the program with the same arguments.\n";
	std::cout << "    restart n: Restart the program with n games.\n";
	std::cout << "    undo: If the command is entered as the guessed word, undo the last complete guess for all\n";
	std::cout << "          games for which it was made. If the command is entered as a response, undo the last\n";
	std::cout << "          only.\n";

	while(this->state != STATE_EXIT)
	{
		this->process();
	}
}
