#ifndef PARALLEL_RDLE_H
#define PARALLEL_RDLE_H

#include <string>
#include <vector>
#include "rdle.h"

/**
 * A class for a game of parallel rdles.
 */
class ParallelRdle
{
	public:
		/** Type for a list of words */
		typedef std::vector<std::string> word_list_t;

		/** The rdle games in parallel */
		std::vector<Rdle> games;

		/**
		 * Constructor.
		 * 
		 * @param num_rdles The number of rdles to play at once.
		 * @param dict_file_path The file path to the dictionary.
		 * @param comparisons_file_path The file path to the comparisons cache.
		 * @param scores_file_path The file path to the scores cache.
		 * @param hard Whether or not to use hard mode. This only works if one game is being played.
		 */
		ParallelRdle(unsigned int num_rdles, std::string dict_file_path, std::string comparisons_file_path, std::string scores_file_path, bool hard);

		/**
		 * Constructor. Assumes the dictionary, comparisons, and scores should all be present in
		 * the directory specified by cache_dir_path, with the names "dictionary.txt",
		 * "comparisons.bin", and "scores.bin".
		 * 
		 * @param num_rdles The number of rdles to play at once.
		 * @param cache_dir_path The file path to the directory containing the caches.
		 * @param hard Whether or not to use hard mode. This only works if one game is being played.
		 */
		ParallelRdle(unsigned int num_rdles, std::string cache_dir_path, bool hard);

		/**
		 * Get the best guesses for this parallel rdle game.
		 * 
		 * @return An ordered list of best guesses, with the best ones first.
		 */
		word_list_t get_best_guesses(void);

		/**
		 * Get the possible words remaining for each game.
		 * 
		 * @return The possible words remaining.
		 */
		std::vector<Rdle::word_set_t> get_possible_words(void);

		/**
		 * Run the command-line interface for a parallel rdle game.
		 */
		void run_cli(void);

	private:
		/** Type for the state of the game */
		typedef enum {
			STATE_PRINT_STATUS,    /**< Print status state */
			STATE_GUESS,           /**< Enter guessed word state */
			STATE_RESPOND,         /**< Enter response state */
			STATE_PROCESS_GUESSES, /**< Process guess and responses state */
			STATE_EXIT             /**< Exit state */
		} state_t;

		/** Path to the dictionary */
		std::string dict_file_path;
		/** Path to the comparisons cache */
		std::string comparisons_file_path;
		/** Path to the scores cache */
		std::string scores_file_path;
		/** Whether or not hard mode is active */
		bool hard;
		/** The command that still needs to be executed */
		std::string pending_command;
		/** The current guess word */
		std::string guess_word;
		/** The current set of responses */
		std::vector<std::string> responses;
		/** The current state of the game */
		state_t state;

		bool parse_command(std::string cmd);
		void undo(void);
		void execute_command(void);
		void enter_guess(void);
		void enter_response(void);
		void process_guesses(void);
		void print_status(void);
		void process(void);
		void init(unsigned int num_rdles, std::string dict_file_path, std::string comparisons_file_path, std::string scores_file_path, bool hard);
};

#endif // PARALLEL_RDLE_H
