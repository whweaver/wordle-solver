#include <iostream>
#include <exception>

#include "rdle.h"
#include "parallel_rdle.h"

int main(int argc, char * * argv) {
	if(argc < 2)
	{
		throw std::runtime_error("No directory provided!");
	}
	std::string dir(argv[1]);

	int num_rdles = (argc >= 3) ? std::stoi(argv[2]) : 1;
	if(num_rdles < 1)
	{
		throw std::runtime_error("Invalid number of rdles");
	}

	ParallelRdle p_rdle((unsigned int)num_rdles, dir, false);
	p_rdle.run_cli();

	return 0;
}
