#ifndef RDLE_H
#define RDLE_H

#include <vector>
#include <unordered_map>
#include <unordered_set>
#include <utility>
#include <string>
#include <cstdint>
#include "work_tracker.h"

/**
 * A wordle-style game.
 */
class Rdle
{
	public:
		/** Type for the length of a dictionary */
		typedef uint_fast32_t dict_length_t;
		/** Type for a word score */
		typedef std::pair<std::string, double> word_score_t;
		/** Type for sorted word scores */
		typedef std::vector<word_score_t> sorted_word_scores_t;
		/** Type for a set of possible words */
		typedef std::unordered_set<std::string> word_set_t;
		/** Type for a word */
		typedef std::string word_t;
		/** Type for a list of words */
		typedef std::vector<word_t> word_list_t;
		/** Type for the cache of comparisons */
		typedef std::vector<std::vector<uint8_t>> comparison_cache_t;
		/** Type for the word scores */
		typedef std::unordered_map<word_t, double> word_scores_t;
		/** Type for the length of a word */
		typedef uint_fast8_t word_length_t;

		/**
		 * Constructor.
		 * 
		 * @param dict_file_path The file path to the dictionary.
		 * @param comparisons_file_path The file path to the comparisons cache.
		 * @param scores_file_path The file path to the scores cache.
		 * @param hard Whether or not to enforce hard mode (guesses must be possible answers).
		 */
		Rdle(std::string dict_file_path, std::string comparisons_file_path, std::string scores_file_path, bool hard);

		/**
		 * Get the best guesses for the current situation.
		 * 
		 * @return The sorted words and their scores.
		 */
		sorted_word_scores_t get_best_guesses(void);

		/**
		 * Submit a guess and response to reduce the number of possible words.
		 * 
		 * @param guess The guessed word.
		 * @param response The response to the guessed word.
		 */
		void guess(std::string guess, std::string response);

		/**
		 * Get the possible words given the current puzzle state.
		 * 
		 * @return The possible words.
		 */
		word_set_t get_possible_words(void);

		/**
		 * Determine if this rdle has been solved.
		 * 
		 * @return True if it has been solved, false otherwise.
		 */
		bool is_solved(void);

		/**
		 * Get the solution to the rdle. If the solution is not known yet,
		 * a zero-length string is returned.
		 * 
		 * @return The solution to the rdle, or a zero-length string.
		 */
		std::string get_solution(void);

		/**
		 * Get the length of a word in this puzzle.
		 * 
		 * @return The length of a word.
		 */
		word_length_t get_word_len(void);

		/**
		 * Undo the last guess.
		 */
		void undo(void);

		/**
		 * Get the number of guesses that have been played so far.
		 * 
		 * @return The number of guesses played so far.
		 */
		unsigned int num_guesses(void);

	private:
		/** Type for a comparison between two words */
		typedef uint_fast32_t comparison_t;
		/** Type for the letter counts */
		typedef std::unordered_map<char, std::pair<word_length_t, word_length_t>> letter_counts_t;
		/** Type for the possible letters */
		typedef std::vector<std::unordered_set<char>> possible_letters_t;
		/** Type for a mapping of words to their dictionary indices */
		typedef std::unordered_map<word_t, dict_length_t> word_map_t;
		/** Type for a line of comparisons */
		typedef std::vector<uint8_t> comparison_line_t;
		/** Type for a list of guesses and responses */
		typedef std::vector<std::pair<std::string, std::string>> guess_list_t;

		/** The file path to the dictionary */
		std::string dict_file_path;
		/** The file path to the comparisons */
		std::string comparisons_file_path;
		/** The file path to the scores */
		std::string scores_file_path;
		/** Whether or not to use hard mode (can only guess words that are possible solutions) */
		bool hard = false;
		/** Possible letters in each character position */
		possible_letters_t possible_letters;
		/** Possible solutions to the puzzle */
		word_map_t possible_words;
		/** Scores for every valid guess word */
		word_scores_t guess_word_scores;
		/** Length of the word to be guessed */
		word_length_t word_len = 0;
		/** The dictionary of all possible words */
		word_list_t * dict = NULL;
		/** Comparisons between all possible words */
		comparison_cache_t * comparisons = NULL;
		/** The correct comparison between two words */
		comparison_t correct_comparison;
		/** The size of a comparison in bits */
		uint_fast8_t comparison_size_bits;
		/** The length of a comparison line in bytes */
		unsigned int comparison_line_length;
		/** Counts of how many of each letter are known to exist in the answer */
		letter_counts_t letter_counts;
		/** The logarithmic divisor to use when calculating scores */
		double log_divisor;
		/** The guesses that have been made so far */
		guess_list_t guesses;
		/** Whether or not this rdle has been solved */
		bool solved;

		// Interface methods
		void reinit(void);
		void parse_response(std::string& guess, std::string& response);
		void recalculate_possible_words(void);

		// Dictionary methods
		void init_dictionary(void);
		void load_dictionary(void);

		// Comparison methods
		void init_comparisons(void);
		uint_fast8_t comparison_size(void);
		void load_comparisons(void);
		void append_comparison(comparison_line_t& comparison_line, dict_length_t poss_word_idx, comparison_t comparison);
		comparison_t extract_comparison(dict_length_t guess_word_idx, dict_length_t poss_word_idx);
		comparison_t compare_words(const word_t& guess_word, const word_t& actual_word);
		void populate_comparison_line(std::string& guess_word, comparison_line_t& comparison_line);
		void comparison_runner(WorkTracker& work_tracker);
		void create_comparisons(void);

		// Scores methods
		void init_scores(void);
		void load_scores(void);
		double guess_word_score(const dict_length_t guess_word_idx);
		void score_runner(std::vector<double>& scores, WorkTracker& work_tracker);
		void calculate_word_scores(void);
		void create_scores(void);
};

#endif // RDLE_H
