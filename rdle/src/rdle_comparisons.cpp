#include "rdle.h"
#include <fstream>
#include <iostream>
#include <filesystem>
#include <map>
#include <cmath>
#include <ctime>
#include <thread>

/** Values to be used for each color of a comparison between two words. @{ */
typedef uint_fast8_t single_comparison_t;
#define NUM_COMPARISONS   3u
#define GREEN_COMPARISON  0u
#define YELLOW_COMPARISON 1u
#define GRAY_COMPARISON   2u
/** @} */

// Cache of comparisons so we don't load a comparisons file more than once.
static std::unordered_map<std::string, Rdle::comparison_cache_t> Comp_Cache;

/**
 * Initialize comparisons.
 */
void Rdle::init_comparisons(void)
{
	// Compute the correct comparison for use later on
	this->correct_comparison = 0u;
	for(word_length_t pos; pos < this->word_len; pos++)
	{
		this->correct_comparison = (this->correct_comparison * NUM_COMPARISONS) + GREEN_COMPARISON;
	}

	// Compute the comparison size for use later on
	unsigned int max_comparison = std::pow(NUM_COMPARISONS, this->word_len);
	for(this->comparison_size_bits = 0u; max_comparison > 0u; this->comparison_size_bits++)
	{
		max_comparison = max_comparison >> 1u;
	}

	// Compute comparison line length for use later on
	this->comparison_line_length = (this->comparison_size_bits * this->dict->size()) >> 3u;

	if(Comp_Cache.count(this->comparisons_file_path) > 0u)
	{
		// Point our comparisons reference to the cache for this comparisons file
		this->comparisons = &Comp_Cache[this->comparisons_file_path];
	}
	else
	{
		// Set up location in the comparisons cache and point our comparisons reference to it
		Comp_Cache[this->comparisons_file_path] = comparison_cache_t();
		this->comparisons = &Comp_Cache[this->comparisons_file_path];

		// Check if comparisons exists
		if(std::filesystem::exists(this->comparisons_file_path))
		{
			// Load the comparisons from disk
			this->load_comparisons();
		}
		else
		{
			// Create comparisons file
			this->create_comparisons();
		}
	}
}

/**
 * Load the comparisons from disk.
 */
void Rdle::load_comparisons(void)
{
	std::cout << "Loading comparison file...\n";

	// Open file for binary writing
	std::ifstream f(this->comparisons_file_path, std::ios::in | std::ios::binary);
	if(!f)
	{
		throw std::runtime_error("Could not open comparisons file for reading!");
	}

	for(dict_length_t guess_word_idx = 0u; guess_word_idx < this->dict->size(); guess_word_idx++)
	{
		this->comparisons->emplace_back(this->comparison_line_length);
		f.read((char*)this->comparisons->back().data(), this->comparison_line_length);
	}

	f.close();
	if(!f.good())
	{
		throw std::runtime_error("Error reading comparisons file!");
	}
}

/**
 * Extract a comparison from the line.
 * 
 * @param guess_word_idx The index of the guess word.
 * @param poss_word_idx The index of the possible word.
 * 
 * @return The relevant comparison.
 */
Rdle::comparison_t Rdle::extract_comparison(dict_length_t guess_word_idx, dict_length_t poss_word_idx)
{
	comparison_line_t& comparison_line = (*this->comparisons)[guess_word_idx];
	unsigned int const total_bit_offset = poss_word_idx * this->comparison_size_bits;
	unsigned int byte_idx = total_bit_offset >> 3u;
	uint8_t bit_idx = total_bit_offset & 0x7u;
	uint8_t bytes_affected = (this->comparison_size_bits + bit_idx + 8u - 1u) >> 3u;

	// Compose comparison from all bytes with even a piece of the comparison.
	uint64_t shifted_comparison = 0u;
	for(uint8_t i = 0u; i < bytes_affected; i++)
	{
		shifted_comparison |= comparison_line[byte_idx + i] << (i * 8u);
	}

	// Discard irrelevant bits from each end of the comparison.
	return (shifted_comparison >> bit_idx) & ((1u << this->comparison_size_bits) - 1u);
}

/**
 * Append comparison to the comparison record. Assumes that no possible words with a
 * higher index have been added and all their comparisons have been initialized to 0.
 * 
 * @param comparison_line The line of comparisons to append to.
 * @param poss_word_idx The index of the possible word.
 * @param comparison The value of the comparison.
 */
void Rdle::append_comparison(comparison_line_t& comparison_line, dict_length_t poss_word_idx, comparison_t comparison)
{
	// Calculate bit and byte offset for the start of the comparison
	unsigned int const total_bit_offset = poss_word_idx * this->comparison_size_bits;
	unsigned int byte_idx = total_bit_offset >> 3u;
	uint8_t bit_idx = total_bit_offset & 0x7u;

	uint8_t bytes_affected = (this->comparison_size_bits + bit_idx + 8u - 1u) >> 3u;
	uint64_t shifted_comparison = comparison << bit_idx;

	// Add first byte, allowing for possibly pre-existing bits to persist.
	comparison_line[byte_idx] |= shifted_comparison & 0xFFu;

	// Add remaining bytes
	for(uint8_t i = 1u; i < bytes_affected; i++)
	{
		comparison_line[byte_idx + i] = (shifted_comparison >> (i * 8u)) & 0xFFu;
	}
}

/**
 * Compare a guessed word against a presumed actual word.
 * 
 * @param guess_word The guessed word.
 * @param actual_word The presumed actual word.
 * 
 * @return A value indicating the result of the comparison.
 */
Rdle::comparison_t Rdle::compare_words(const word_t& guess_word, const word_t& actual_word)
{
	const word_length_t guess_word_len = guess_word.length();

	std::vector<single_comparison_t> comparison(guess_word_len, GRAY_COMPARISON);

	// Count the letters in the actual word
	std::map<char, word_length_t> letter_counts;
	for(const char c : actual_word)
	{
		if(letter_counts.count(c) > 0)
		{
			letter_counts[c]++;
		}
		else
		{
			letter_counts[c] = 1u;
		}
	}

	// Check for green matches first
	for(word_length_t pos = 0u; pos < guess_word_len; pos++)
	{
		// If the letter in the guess word is in the right place
		if(guess_word[pos] == actual_word[pos])
		{
			// Mark the comparison green
			comparison[pos] = GREEN_COMPARISON;
			
			// Mark that we've used one instance of the green letter
			// Note that we don't have to check if this letter exists because
			// we already confirmed there's an instance of it in the exact
			// comparison.
			letter_counts[guess_word[pos]]--;
		}
	}

	// Check for yellow matches next
	for(word_length_t pos = 0u; pos < guess_word_len; pos++)
	{
		// Get the character from the guess word */
		char c = guess_word[pos];

		// If that character isn't already green AND the letter exists in the actual word AND we haven't used up those letters yet
		if((comparison[pos] != GREEN_COMPARISON) && (letter_counts.count(c) > 0) && (letter_counts[c] > 0u))
		{
			// Mark the comparison as yellow
			comparison[pos] = YELLOW_COMPARISON;

			// Mark that we've used one of the instances of that letter.
			letter_counts[c]--;
		}
	}

	// Convert the list of comparisons into a single value
	comparison_t comp_val = 0u;
	for(const single_comparison_t c : comparison)
	{
		comp_val = (comp_val * NUM_COMPARISONS) + c;
	}

	return comp_val;
}

/**
 * Populate a comparison line, assuming it's been allocated already.
 * 
 * @param guess_word The word for which the line is being populated.
 * @param comparison_line The line to populate.
 */
void Rdle::populate_comparison_line(std::string& guess_word, comparison_line_t& comparison_line)
{
	for(dict_length_t word_idx = 0u; word_idx < this->dict->size(); word_idx++)
	{
		comparison_t comparison = this->compare_words(guess_word, (*this->dict)[word_idx]);
		this->append_comparison(comparison_line, word_idx, comparison);
	}
}

/**
 * Function for running comparisons in a thread.
 * 
 * @param work_tracker The work tracker for this parallel section.
 */
void Rdle::comparison_runner(WorkTracker& work_tracker)
{
	dict_length_t guess_word_idx = work_tracker.get_work();

	while(guess_word_idx < this->dict->size())
	{
		// Do the work
		this->populate_comparison_line((*this->dict)[guess_word_idx], (*this->comparisons)[guess_word_idx]);

		// Report work done
		work_tracker.report_work_done(guess_word_idx);

		// Get the next work
		guess_word_idx = work_tracker.get_work();
	}
}

/**
 * Create all comparisons using concurrency.
 */
void Rdle::create_comparisons(void)
{
	std::cout << "Generating comparison file...\n";

	// Create all comparison lines and allocate the space for them
	for(dict_length_t i = 0u; i < this->dict->size(); i++)
	{
		this->comparisons->emplace_back(this->comparison_line_length, 0u);
	}

	// Open file for binary writing
	std::ofstream f(this->comparisons_file_path, std::ios::out | std::ios::binary);
	if(!f) {
		throw std::runtime_error("Could not open comparisons file for writing!");
	}

	// Start threads
	unsigned int const num_threads = std::thread::hardware_concurrency();
	WorkTracker work_tracker(this->dict->size(), num_threads, 2u * CLOCKS_PER_SEC * num_threads);
	std::vector<std::thread> threads(num_threads);
	for(unsigned int thread_idx = 0u; thread_idx < threads.size(); thread_idx++)
	{
		threads[thread_idx] = std::thread(&Rdle::comparison_runner, this, std::ref(work_tracker));
	}

	// Write comparison values to disk as we go
	for(dict_length_t write_idx = 0u; write_idx < this->dict->size(); write_idx++)
	{
		work_tracker.wait_for_done(write_idx);

		// Write comparison line to file
		f.write((const char*)(*this->comparisons)[write_idx].data(), (*this->comparisons)[write_idx].size());

		// Report time
		work_tracker.print_progress(write_idx);
	}

	// Join threads
	for(auto& thread : threads)
	{
		thread.join();
	}

	f.close();
	if(!f.good())
	{
		throw std::runtime_error("Error writing comparisons file!");
	}
}
