#include "rdle.h"
#include <fstream>
#include <iostream>

// Cache of dictionaries so we don't load a dictionary file more than once.
static std::unordered_map<std::string, Rdle::word_list_t> Dict_Cache;

void Rdle::load_dictionary(void)
{
	std::cout << "Loading dictionary...\n";

	// Set up location in the dictionary cache and point our dictionary reference to it
	Dict_Cache[this->dict_file_path] = word_list_t();
	this->dict = &Dict_Cache[this->dict_file_path];

	// Open file for reading
	std::ifstream dict_file(this->dict_file_path);

	// Ensure at least one word exists
	word_t word;
	if(!std::getline(dict_file, word))
	{
		throw std::out_of_range("No words in dictionary!");
	}

	// Need to define a word_len and setup possible_letters before processing any words.
	this->word_len = word.length();
	this->dict->push_back(word);

	// Process the rest of the words
	for(; std::getline(dict_file, word); ) 
	{
		// Validate word
		if(word.length() != this->word_len)
		{
			throw std::length_error("Not all dictionary words are the same length!");
		}
		this->dict->push_back(word);
	}
}

/**
 * Load the dictionary from disk.
 */
void Rdle::init_dictionary(void)
{
	if(Dict_Cache.count(this->dict_file_path) > 0u)
	{
		// Point our dictionary reference to the cache for this dictionary
		this->dict = &Dict_Cache[this->dict_file_path];
		this->word_len = this->dict->begin()->length();
	}
	else
	{
		this->load_dictionary();
	}

	// Set up possible letters
	this->possible_letters.resize(this->word_len);
	for(auto& poss_letters_at_pos : this->possible_letters)
	{
		poss_letters_at_pos.clear();
	}
	for(std::string& word : *this->dict)
	{
		for(word_length_t pos = 0u; pos < word.length(); pos++)
		{
			this->possible_letters[pos].insert(word[pos]);
		}
	}

	// Copy all dictionary words into the possible words, along with a reference to the dictionary index for the word.
	this->possible_words = word_map_t();
	for(dict_length_t i = 0u; i < this->dict->size(); i++)
	{
		this->possible_words[(*this->dict)[i]] = i;
	}
}
