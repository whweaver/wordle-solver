#include "rdle.h"
#include <algorithm>
#include <iostream>

/** A sentinel value for an unknown letter count */
#define LETTER_COUNT_NONE ((word_length_t)-1)

/**
 * Compare the scores of two words.
 * 
 * @param a The first word/score pair.
 * @param b The second word/score pair.
 * 
 * @return True if a has a lower score than b, false otherwise.
 */
static bool compare_scores(Rdle::word_score_t& a, Rdle::word_score_t& b)
{
	return a.second < b.second;
}

/**
 * Reinitialize to a blank slate.
 */
void Rdle::reinit(void)
{
	this->letter_counts = letter_counts_t();
	this->guesses = guess_list_t();
	this->solved = false;

	// Load the dictionary from disk
	this->init_dictionary();

	// Initialize comparisons
	this->init_comparisons();

	// Initialize scores
	this->init_scores();
}

Rdle::Rdle(std::string dict_file_path, std::string comparisons_file_path, std::string scores_file_path, bool hard)
{
	this->dict_file_path = dict_file_path;
	this->comparisons_file_path = comparisons_file_path;
	this->scores_file_path = scores_file_path;
	this->hard = hard;

	this->reinit();
}

Rdle::sorted_word_scores_t Rdle::get_best_guesses(void)
{
	sorted_word_scores_t sorted_word_scores;

	for(auto& kv : this->guess_word_scores)
	{
		sorted_word_scores.push_back(kv);
	}

	std::sort(sorted_word_scores.begin(), sorted_word_scores.end(), compare_scores);

	return sorted_word_scores;
}

Rdle::word_set_t Rdle::get_possible_words(void)
{
	word_set_t ret_words;

	for(auto& [word, dict_idx] : this->possible_words)
	{
		ret_words.insert(word);
	}

	return ret_words;
}

/**
 * Parse response into must-have letters and possible letters at given locations.
 * 
 * @param guess The guessed word.
 * @param response The response to the guessed word.
 */
void Rdle::parse_response(std::string& guess, std::string& response)
{
	std::unordered_map<char, word_length_t> new_letter_counts;
	bool is_solution = true;

	// Check for green and yellow letters
	for(word_length_t pos = 0u; pos < response.size(); pos++)
	{
		char const response_code = std::toupper(response[pos]);
		char const c = std::toupper(guess[pos]);

		if(response_code == '_')
		{
			// We'll deal with grays later
			is_solution = false;
			continue;
		}
		else if(response_code == 'G')
		{
			// Letter is green
			// Set position to be only this green letter
			this->possible_letters[pos].clear();
			this->possible_letters[pos].insert(c);

			// Add letter to must haves
			if(new_letter_counts.count(c) > 0u)
			{
				new_letter_counts[c]++;
			}
			else
			{
				new_letter_counts[c] = 1u;
			}
		}
		else if(response_code == 'Y')
		{
			// Letter is yellow
			is_solution = false;

			// Add letter to must-haves
			if(new_letter_counts.count(c) > 0u)
			{
				new_letter_counts[c]++;
			}
			else
			{
				new_letter_counts[c] = 1u;
			}

			// Remove letter from this position
			this->possible_letters[pos].erase(c);
		}
		else
		{
			// Unknown letter
			throw;
		}
	}

	// Check to see if we need more of a given letter than we previously thought
	for(auto& [c, count] : new_letter_counts)
	{
		if(this->letter_counts.count(c) == 0u)
		{
			this->letter_counts[c] = std::make_pair(new_letter_counts[c], LETTER_COUNT_NONE);
		}
		else if(new_letter_counts[c] > this->letter_counts[c].first)
		{
			this->letter_counts[c].first = new_letter_counts[c];
		}
		else
		{
			// This guess has fewer of this character than we thought before.
			// Always take the higher character count.
		}
	}

	// Check for gray letters
	for(word_length_t pos = 0u; pos < response.size(); pos++)
	{
		if(response[pos] == '_')
		{
			char const c = std::toupper(guess[pos]);

			// Remove letter from this position
			this->possible_letters[pos].erase(c);

			// Set maximum number of letter occurrences if this letter is a must-have
			if(this->letter_counts.count(c) > 0u)
			{
				this->letter_counts[c].second = this->letter_counts[c].first;
			}
			// Otherwise remove this letter from other positions
			else
			{
				for(auto& pos_poss_letters : this->possible_letters)
				{
					pos_poss_letters.erase(c);
				}
			}
		}
	}

	// Check if we've found the solution
	if(is_solution)
	{
		this->solved = true;
	}
}

/**
 * Recalculate possible words from must-have letters and possible letters.
 */
void Rdle::recalculate_possible_words(void)
{
	word_map_t new_possible_words;

	for(auto& [poss_word, dict_idx] : this->possible_words)
	{
		// Count all letters in the possible word
		std::unordered_map<char, word_length_t> poss_word_letter_counts;
		for(char c : poss_word)
		{
			if(poss_word_letter_counts.count(c) > 0u)
			{
				poss_word_letter_counts[c]++;
			}
			else
			{
				poss_word_letter_counts[c] = 1u;
			}
		}

		// Check that all the must-have letters are present in the right quantities
		bool is_still_possible = true;
		for(auto& [c, letter_count] : this->letter_counts)
		{
			word_length_t poss_word_letter_count = (poss_word_letter_counts.count(c) > 0u) ? poss_word_letter_counts[c] : 0u;
			if((
					(letter_count.first != LETTER_COUNT_NONE) &&
					(poss_word_letter_count < letter_count.first)
			   ) ||
			   (
					(letter_count.second != LETTER_COUNT_NONE) &&
					(poss_word_letter_count > letter_count.second)
			   ))
			{
				is_still_possible = false;
				break;
			}
		}

		// Check that each position only has allowable letters for that position
		for(word_length_t pos = 0u; pos < poss_word.size(); pos++)
		{
			if(this->possible_letters[pos].count(poss_word[pos]) == 0u)
			{
				is_still_possible = false;
				break;
			}
		}

		if(is_still_possible)
		{
			new_possible_words[poss_word] = dict_idx;
		}
	}

	this->possible_words = new_possible_words;
}

void Rdle::guess(std::string guess, std::string response)
{
	// Parse response
	this->parse_response(guess, response);	

	// Recalculate possible words from parsed response
	this->recalculate_possible_words();

	// Recalculate new best word scores
	this->calculate_word_scores();

	// Save guess for later
	this->guesses.emplace_back(guess, response);
}

bool Rdle::is_solved(void)
{
	return this->solved;
}

std::string Rdle::get_solution(void)
{
	std::string ret_val = "";

	if(this->possible_words.size() == 1u)
	{
		ret_val = this->possible_words.begin()->first;
	}

	return ret_val;
}

Rdle::word_length_t Rdle::get_word_len(void)
{
	return this->word_len;
}

void Rdle::undo(void)
{
	// Remove the last guess
	this->guesses.pop_back();
	guess_list_t old_guesses = this->guesses;

	// Reinitialize everything
	this->reinit();

	// Recalculating all this with no guesses usually takes a long time
	// because it's ignoring the cached initial state, so we can skip it in
	// that case.
	if(old_guesses.size() > 0u)
	{
		// Reapply all the guesses
		for(auto& [guess, response] : old_guesses)
		{
			this->parse_response(guess, response);
			this->guesses.emplace_back(guess, response);
		}

		// Recalculate possible words and word scores
		this->recalculate_possible_words();
		this->calculate_word_scores();
	}
}

unsigned int Rdle::num_guesses(void)
{
	return this->guesses.size();
}
