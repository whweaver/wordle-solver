#include "rdle.h"
#include <fstream>
#include <iostream>
#include <filesystem>
#include <map>
#include <cmath>
#include <ctime>
#include <thread>

// Cache of scores so we don't load a scores file more than once.
static std::unordered_map<std::string, Rdle::word_scores_t> Scores_Cache;

/**
 * Initialize scores.
 */
void Rdle::init_scores(void)
{
	// These games usually require 6 guesses, so we're going to assume that with good
	// strategy, it take 3 guesses to find the right word, with the 4th guess being the
	// word. This means that we need to divide the number of words by some number 3 times
	// to get 1, which can be represented as follows:
	// N / x^3 = 1
	// N^(1/3) = x
	const double divisor = std::pow(this->dict->size(), 1.0d / 3.0d);

	// Given that we're somewhere in the middle of guessing, we have some number of words
	// remaining, n. If we take the divisor assumption x above, then the number of expected
	// moves from here to the solution is log_x(n). To compute this, we will use the log
	// identity log_x(n) = ln(n) / ln(x), so we can pre-compute the divisor ln(x).
	this->log_divisor = std::log(divisor);
	
	if(Scores_Cache.count(this->scores_file_path) > 0u)
	{
		// Copy the scores cache
		this->guess_word_scores = Scores_Cache[this->scores_file_path];
	}
	else
	{
		// Check if scores exists
		if(std::filesystem::exists(this->scores_file_path))
		{
			// Load the scores from disk
			this->load_scores();
		}
		else
		{
			// Create scores file
			this->create_scores();
		}

		// Copy the scores into the cache
		Scores_Cache[scores_file_path] = this->guess_word_scores;
	}
}

/**
 * Load scores from file.
 */
void Rdle::load_scores(void)
{
	std::cout << "Loading scores...\n";

	// Open file for binary writing
	std::ifstream f(this->scores_file_path, std::ios::in | std::ios::binary);
	if(!f)
	{
		throw std::runtime_error("Could not open scores file for reading!");
	}

	this->guess_word_scores.clear();
	for(std::string guess_word : *this->dict)
	{
		double score = 0.0d;
		f.read((char*)&score, sizeof(score));
		this->guess_word_scores[guess_word] = score;
	}

	f.close();
	if(!f.good())
	{
		throw std::runtime_error("Error reading scores file!");
	}
}

/**
 * Calculate a word's score as a guessing word for a given list of possible words.
 * 
 * @param guess_word_idx The index of the word to guess.
 * 
 * @return The guess word's score.
 */
double Rdle::guess_word_score(const dict_length_t guess_word_idx)
{
	std::map<comparison_t, unsigned int> comparisons_aggregate;

	// Calculate the number of possible words with each comparison
	for(auto& [poss_word, dict_idx] : this->possible_words)
	{
		comparison_t comparison = this->extract_comparison(guess_word_idx, dict_idx);
		comparisons_aggregate.try_emplace(comparison, 0u);
		comparisons_aggregate[comparison]++;
	}

	// The expected number of moves is 0 for the correct comparison, so it can be removed
	// if it exists.
	comparisons_aggregate.erase(this->correct_comparison);

	// Calculate the number of expected moves remaining if using this guess word.
	double expected_moves = 0.0d;
	const double num_possible_words = this->possible_words.size();
	for(const auto& [comparison, count] : comparisons_aggregate)
	{
		// This is the fraction of the probability space that this comparison occupies,
		// or in other words, the probability that this comparison is what results from
		// using this guess word.
		const double frac_of_total_space = count / num_possible_words;

		// This is the number of expected moves remaining if this guess word is used.
		// One is added because of the base case: if there's a single word in this
		// comparison (and it's not the correct comparison), then log(1) will be 0
		// but another move is needed in order to actually guess that correct word.
		const double comparison_exp_moves = (std::log(count) / this->log_divisor) + 1.0d;

		// Weight the number of expected moves according to this comparison's
		// probability.
		expected_moves += frac_of_total_space * comparison_exp_moves;
	}

	return expected_moves;
}

/**
 * Function for running scores in a thread.
 * 
 * @param scores Scores for all guess words.
 * @param work_tracker Work tracker for this parallel section.
 */
void Rdle::score_runner(std::vector<double>& scores, WorkTracker& work_tracker)
{
	dict_length_t guess_word_idx = work_tracker.get_work();

	while(guess_word_idx < scores.size())
	{
		// Do the work
		scores[guess_word_idx] = this->guess_word_score(guess_word_idx);

		// Report work done
		work_tracker.report_work_done(guess_word_idx);

		// Get the next work
		guess_word_idx = work_tracker.get_work();
	}
}

/**
 * Create all word scores using concurrency.
 */
void Rdle::calculate_word_scores(void)
{
	// Create all score slots and allocate the space for them
	std::vector<double> word_scores(this->dict->size(), 0.0d);

	// Start threads
	unsigned int const num_threads = std::thread::hardware_concurrency();
	WorkTracker work_tracker(this->dict->size(), num_threads, 2u * CLOCKS_PER_SEC * num_threads);
	std::vector<std::thread> threads(num_threads);
	for(unsigned int thread_idx = 0u; thread_idx < threads.size(); thread_idx++)
	{
		threads[thread_idx] = std::thread(&Rdle::score_runner, this, std::ref(word_scores), std::ref(work_tracker));
	}

	// Write comparison values to map as we go
	this->guess_word_scores.clear();
	for(dict_length_t write_idx = 0u; write_idx < this->dict->size(); write_idx++)
	{
		// Wait for the current index to be done.
		work_tracker.wait_for_done(write_idx);

		this->guess_word_scores[(*this->dict)[write_idx]] = word_scores[write_idx];

		// Report time
		work_tracker.print_progress(write_idx);
	}

	// Join threads
	for(auto& thread : threads)
	{
		thread.join();
	}
}

/**
 * Generate scores and save to file.
 */
void Rdle::create_scores(void)
{
	std::cout << "Generating scores...\n";

	this->calculate_word_scores();

	// Open file for binary writing
	std::ofstream f(this->scores_file_path, std::ios::out | std::ios::binary);
	if(!f) {
		throw std::runtime_error("Could not open scores file for writing!");
	}

	for(std::string guess_word : *this->dict)
	{
		const double score = this->guess_word_scores[guess_word];
		f.write((char*)&score, sizeof(score));
	}

	f.close();
	if(!f.good())
	{
		throw std::runtime_error("Error writing scores file!");
	}
}
