#include "work_tracker.h"
#include <vector>
#include <mutex>
#include <ctime>
#include <iostream>

/**
 * Constructor.
 * 
 * @param num_jobs The number of jobs to track.
 * @param num_threads The number of threads performing those jobs.
 * @param progress_update_interval The number of clock cycles between progress updates.
 */
WorkTracker::WorkTracker(unsigned int num_jobs, unsigned int num_threads, clock_t progress_update_interval)
{
	this->available_idx = 0u;
	this->done_status = std::vector<bool>(num_jobs, false);
	this->start_time = clock();
	this->num_threads = num_threads;
	this->progress_update_interval = progress_update_interval;
	this->last_update_time = this->start_time;
}

/**
 * Get the index of an unassigned job to work.
 * 
 * @return The index of the job to work.
 */
unsigned int WorkTracker::get_work(void)
{
	std::lock_guard<std::mutex> lock(this->available_mutex);
	unsigned int work = this->available_idx;
	this->available_idx++;
	return work;
}

/**
 * Report a job done.
 * 
 * @param work_idx The index of the job that's done.
 */
void WorkTracker::report_work_done(unsigned int work_idx)
{
	std::lock_guard<std::mutex> lock(this->done_status_mutex);
	this->done_status[work_idx] = true;
}

/**
 * Wait for the given job to be done.
 * 
 * @param idx Index of the job to wait for.
 */
void WorkTracker::wait_for_done(unsigned int idx)
{
	// Read access to std::vector<bool> is thread-safe.
	while(!this->done_status[idx])
	{
	}
}

/**
 * Print the progress through the work.
 * 
 * @param finalized_idx Assume this is the index of the most recent job finalized.
 */
void WorkTracker::print_progress(unsigned int finalized_idx)
{
	clock_t cur_time = clock();
	if(cur_time > (this->last_update_time + this->progress_update_interval))
	{
		clock_t elapsed_clocks = cur_time - start_time;
		double elapsed_secs = (((double)elapsed_clocks) / ((double)CLOCKS_PER_SEC)) / ((double)this->num_threads);
		double jobs_per_sec = finalized_idx / elapsed_secs;
		unsigned int remaining_jobs = this->done_status.size() - finalized_idx;
		double remaining_secs = remaining_jobs / jobs_per_sec;
		std::cout << finalized_idx << " jobs processed\n";
		std::cout << elapsed_secs << " seconds elapsed\n";
		std::cout << jobs_per_sec << " jobs per sec\n";
		std::cout << remaining_jobs << " jobs remaining\n";
		std::cout << remaining_secs << " seconds remaining\n";
		std::cout << "-----------------------\n";
		this->last_update_time = cur_time;
	}
}
