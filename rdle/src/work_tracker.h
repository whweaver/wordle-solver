#ifndef WORK_TRACKER_H
#define WORK_TRACKER_H

#include <vector>
#include <mutex>
#include <condition_variable>

/**
 * A class to track the work being done by multiple threads, assuming that the threads
 * are doing work that can be defined by consecutive indices.
 */
class WorkTracker
{
	public:
		/**
		 * Constructor.
		 * 
		 * @param num_jobs The number of jobs to track.
		 * @param num_threads The number of threads performing those jobs.
		 * @param progress_update_interval The number of clock cycles between progress updates.
		 */
		WorkTracker(unsigned int num_jobs, unsigned int num_threads, clock_t progress_update_interval);

		/**
		 * Get the index of an unassigned job to work.
		 * 
		 * @return The index of the job to work.
		 */
		unsigned int get_work(void);

		/**
		 * Report a job done.
		 * 
		 * @param work_idx The index of the job that's done.
		 */
		void report_work_done(unsigned int work_idx);

		/**
		 * Wait for the given job to be done.
		 * 
		 * @param idx Index of the job to wait for.
		 */
		void wait_for_done(unsigned int idx);

		/**
		 * Print the progress through the work.
		 * 
		 * @param finalized_idx Assume this is the index of the most recent job finalized.
		 */
		void print_progress(unsigned int finalized_idx);

	private:
		/** The index of the next available job to work */
		unsigned int available_idx;
		/** A mutex controlling access to the next available job to work */
		std::mutex available_mutex;
		/** Whether or not every job is done */
		std::vector<bool> done_status;
		/** A mutex controlling access to the job done status */
		std::mutex done_status_mutex;
		/** The time the work was started */
		clock_t start_time;
		/** The number of threads doing work */
		unsigned int num_threads;
		/** The interval between progress updates */
		clock_t progress_update_interval;
		/** The time of the last update */
		clock_t last_update_time;
};

#endif // WORK_TRACKER_H
