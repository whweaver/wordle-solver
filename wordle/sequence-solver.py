#!/usr/bin/python

import sys
from wordle import Wordle

def get_response():
	while True:
		response = input(f"Response: ")
		if response.lower() == "exit":
			exit()
		if len(response) != 5:
			print("Response must be 5 letters.")
			continue
		for pos, char in enumerate(response):
			if (char.upper() != entered_word[pos]) and (char != '_'):
				print("It looks like this is not a valid response for the word you entered. Please enter the correct response:")
				break
		else:
			return response

hard = False
num_wordles = 1
if len(sys.argv) == 2:
	if sys.argv[1] == 'hard':
		hard = True
		num_wordles = 1
	else:
		num_wordles = int(sys.argv[1])
		if num_wordles <= 0:
			raise f"Invalid number of wordles: {sys.argv[1]}"
elif len(sys.argv) > 2:
	raise "Too many arguments!"

print("Welcome to the Wordle solver!")
print("At each step, input Wordle's response using the following rules:")
print("1) For letters not present, input '_'")
print("2) For letters present but in the wrong location, input the letter in lowercase, i.e. 'r'")
print("3) For letters in the correct location, input the letter in uppercase, i.e. 'S'")
print("An entire response should have 5 characters. For example, if your word was SONAR, your")
print("response might be __nA_, if the n was yellow, the A was green, and the other letters were")
print("gray.")
print("Enter 'exit' to exit at any time.")

entered_words = []
wordle = Wordle(hard)
wordle_idx = 0

while True:

	poss_words = wordle.possible_words()
	print(f"There are {len(poss_words)} possible words.")
	if len(poss_words) <= 100:
		print(" ".join(poss_words))

	if len(poss_words) < 1:
		print(f"No matching word found!")
	elif len(poss_words) == 1:
		print(f"The word is: {poss_words[0]}")
		if entered_words[-1] != poss_words[0]:
			entered_words.append(poss_words[0])
	else:
		print(f"The top ten guessing words are:")
		print(" ".join(wordle.best_guesses()[0:10]))

		while True:
			entered_word = input("Entered word: ").upper()
			if entered_word.lower() == "exit":
				exit()
			if len(entered_word) != 5:
				print("Entered word must be 5 letters.")
			elif ('_' in entered_word) or not ((entered_word.upper() == entered_word) or (entered_word.lower() == entered_word)):
				print("It looks like this is a response, not a word. Please enter the word you entered:")
			else:
				break
		entered_words.append(entered_word)
		response = get_response()
		wordle.guess(entered_word, response)

	if len(poss_words) <= 1:
		wordle_idx += 1
		if wordle_idx >= num_wordles:
			exit()

		print("--------------\n- NEW PUZZLE -\n--------------")
		wordle = Wordle(hard)
		for entered_word in entered_words:
			print(f"Entered word: {entered_word}")
			response = get_response()
			wordle.guess(entered_word, response)

			if len(wordle.possible_words()) == 1:
				break
