#!/usr/bin/python

import sys
from wordle import Wordle

def print_best_guesses():
	print("--------------------------------------------------------------------------------")
	max_poss_words = 0
	best_guesses = {}
	for wordle in wordles:
		poss_words = wordle["game"].possible_words()
		if len(poss_words) > max_poss_words:
			max_poss_words = len(poss_words)
		print(f"There are {len(poss_words)} possible words.")
		if len(poss_words) <= 100:
			print(" ".join(poss_words))

		if len(poss_words) == 1:
			wordle["solution"] = poss_words[0]
		else:
			bg = wordle["game"].best_guesses(True)
			for word, score in bg:
				if word in best_guesses:
					if score > best_guesses[word]:
						best_guesses[word] = score
				else:
					best_guesses[word] = score

	print(f"The top ten guessing words are:")
	top_ten_best_guesses = []
	for wordle in wordles:
		if wordle["solution"] and not wordle["solved"]:
			top_ten_best_guesses.append(wordle["solution"])
	sorted_best_guesses = sorted(best_guesses.items(), key=lambda x: x[1])
	top_ten_best_guesses += list(map(lambda x: x[0], sorted_best_guesses))
	print(" ".join(top_ten_best_guesses[0:10]))

	return max_poss_words

def get_entered_word():
	while True:
		entered_word = input("Entered word: ").upper()
		if entered_word.lower() == "exit":
			return "exit"
		elif entered_word.lower() == "restart":
			return "restart"
		elif entered_word.lower() == "undo":
			return "undo"
		elif len(entered_word) != 5:
			print("Entered word must be 5 letters.")
		elif ('_' in entered_word) or not ((entered_word.upper() == entered_word) or (entered_word.lower() == entered_word)):
			print("It looks like this is a response, not a word. Please enter the word you entered:")
		else:
			return entered_word

def get_response():
	while True:
		response = input(f"Response {i+1}: ")
		if response.lower() == "exit":
			return "exit"
		elif response.lower() == "restart":
			return "restart"
		elif response.lower() == "undo":
			return "undo"
		elif len(response) != 5:
			print("Response must be 5 letters.")
			continue

		for pos, char in enumerate(response):
			if (char.upper() != entered_word[pos]) and (char != '_'):
				print("It looks like this is not a valid response for the word you entered. Please enter the correct response:")
				break
		else:
			return response

def undo(wordles):
	print("----------------------------------------------------------------------------")
	print("Undoing last guess...")
	min_guesses = len(wordles[0]["guesses"])
	max_guesses = len(wordles[0]["guesses"])
	for wordle in wordles:
		num_guesses = len(wordle["guesses"])
		if num_guesses < min_guesses:
			min_guesses = num_guesses
		if num_guesses > max_guesses:
			max_guesses = num_guesses

	if min_guesses == max_guesses:
		target_guesses = min_guesses - 1
	else:
		target_guesses = min_guesses

	for wordle in wordles:
		wordle["guesses"] = wordle["guesses"][0:target_guesses]

hard = False
num_wordles = 1
if len(sys.argv) == 2:
	if sys.argv[1] == 'hard':
		hard = True
		num_wordles = 1
	else:
		num_wordles = int(sys.argv[1])
		if num_wordles <= 0:
			raise f"Invalid number of wordles: {sys.argv[1]}"
elif len(sys.argv) > 2:
	raise "Too many arguments!"
state = None

print("Welcome to the Wordle solver!")
print("At each step, input Wordle's response using the following rules:")
print("    For letters not present, input '_'")
print("    For letters present but in the wrong location, input the letter in lowercase, i.e. 'r'")
print("    For letters in the correct location, input the letter in uppercase, i.e. 'S'")
print("An entire response should have 5 characters. For example, if your word was SONAR, your")
print("response might be __nA_, if the n was yellow, the A was green, and the other letters were")
print("gray.")
print("")
print("You may enter special commands at any time as well:")
print("    exit: Exit the program.")
print("    restart: Restart the program with the same arguments.")
print("    undo: Undo the last guess. If the command is entered as the entered word, it undoes the")
print("          previous entered word. If the command is entered as a response, it undoes the entered")
print("          word for which the response is being entered. This undo applies to all wordles in the")
print("          game simultaneously.")

while True:
	if state == "retry":
		for wordle in wordles:
			wordle["game"] = Wordle(hard)
			for guess in wordle["guesses"]:
				print(f"Applying guess \"{guess['guess']}\" with response \"{guess['response']}\"...")
				wordle["game"].guess(guess["guess"], guess["response"])
	else:
		print("----------------------------------------------------------------------------")
		wordles = []
		for i in range(num_wordles):
			wordles.append({
				"game": Wordle(hard),
				"solution": None,
				"solved": False,
				"guesses": []
			})
	state = None

	while True:

		print_best_guesses()

		entered_word = get_entered_word()

		if entered_word == "exit":
			exit()
		elif entered_word == "restart":
			state = "restart"
		elif entered_word == "undo":
			undo(wordles)
			state = "retry"
		else:
			for i, wordle in enumerate(wordles):
				if entered_word == wordle["solution"]:
					wordle["solved"] = True

				if wordle["solution"] is None:
					response = get_response()

					if response == "exit":
						exit()
					elif response == "restart":
						state = "restart"
						break
					elif response == "undo":
						undo(wordles)
						state = "retry"
						break

					if response == entered_word.upper():
						wordle["solution"] = response
						wordle["solved"] = True

					wordle["game"].guess(entered_word, response)

					wordle["guesses"].append({"guess": entered_word, "response": response})

		if state != None:
			break
