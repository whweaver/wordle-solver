from multiprocessing import Lock
import os
import json
import math

class Wordle:
	__ALPHABET = [*"ABCDEFGHIJKLMNOPQRSTUVWXYZ"]

	__DICTIONARY_FILE = 'dictionary.json'
	__COMPARISON_FILE = 'comparisons.json'
	__PARTITIONS_FILE = 'partitions.json'

	__dictionary = None
	__comparisons = None
	__partitions = None

	def __load_dictionary(self):
		if Wordle.__dictionary is None:
			if os.path.isfile(Wordle.__DICTIONARY_FILE):
				print("Loading dictionary...")
				with open(Wordle.__DICTIONARY_FILE) as f:
					Wordle.__dictionary = json.load(f)

			else:
				print("Computing dictionary...")
				Wordle.__dictionary = {}
				idx = 0
				with open('dictionary.txt') as f:
					for line in f:
						word = line.strip()
						if len(word) == 5:
							Wordle.__dictionary[word] = idx
							idx += 1

				with open(Wordle.__DICTIONARY_FILE, 'w') as f:
					json.dump(Wordle.__dictionary, f)

	def __load_comparisons(self):
		if Wordle.__comparisons is None:
			if os.path.isfile(Wordle.__COMPARISON_FILE):
				print("Loading comparisons...")
				with open(Wordle.__COMPARISON_FILE) as f:
					Wordle.__comparisons = json.load(f)

			else:
				print("Computing comparisons...")
				Wordle.__comparisons = []
				for word in self.__dictionary:
					Wordle.__comparisons.append(self.__preprocess_word(word))

				with open(Wordle.__COMPARISON_FILE, 'w') as f:
					json.dump(Wordle.__comparisons, f)

	def __load_partitions(self):
		if Wordle.__partitions is None:
			if os.path.isfile(Wordle.__PARTITIONS_FILE):
				print("Loading partitions...")
				with open(Wordle.__PARTITIONS_FILE) as f:
					Wordle.__partitions = json.load(f)

			else:
				print("Computing partitions...")
				Wordle.__partitions = self.__calculate_word_scores()

				with open(self.__PARTITIONS_FILE, 'w') as f:
					json.dump(Wordle.__partitions, f)

	def __init__(self, hard=False):
		self.__HARD = hard
		self.__letter_counts = {}
		self.__possible_letters = []
		for i in range(0, 5):
			self.__possible_letters.append(Wordle.__ALPHABET.copy())

		self.__load_dictionary()
		self.__possible_words = Wordle.__dictionary.copy()

		self.__load_comparisons()
		self.__load_partitions()

		self.__guess_word_scores = Wordle.__partitions

	def __guess(self, guess_word, actual_word):
		comparison = ['_'] * 5

		# Find how many of each letter there are in the actual word
		available_letter_counts = {}
		for letter in actual_word:
			if letter not in available_letter_counts:
				available_letter_counts[letter] = 0
			available_letter_counts[letter] += 1

		# Check for exact matches first
		# Doing this first ensures yellows don't appear for letters that are marked green elsewhere
		for pos, letter in enumerate(guess_word):
			if actual_word[pos] == letter:
				comparison[pos] = 'G'
				available_letter_counts[letter] -= 1

		# Check for other letters that are present but in the wrong spot
		for pos, letter in enumerate(guess_word):
			# Only mark a letter as yellow if it is not already green and if there is more of that
			# letter elsewhere in the word
			if (comparison[pos] != 'G') and (letter in available_letter_counts) and (available_letter_counts[letter] > 0):
				comparison[pos] = 'y'
				available_letter_counts[letter] -= 1

		return "".join(comparison)

	def __preprocess_word(self, guess_word):
		lookup_table = []
		for actual_word in Wordle.__dictionary:
			lookup_table.append(self.__guess(guess_word, actual_word))
		return lookup_table

	def __aggregate_comparisons(self, word):
		dictionary = Wordle.__dictionary
		partition = {}
		for poss_word in self.__possible_words:
			guess_idx = dictionary[word]
			poss_idx = dictionary[poss_word]
			comparison = Wordle.__comparisons[guess_idx][poss_idx]
			if comparison not in partition:
				partition[comparison] = 1
			else:
				partition[comparison] += 1
		return partition

	def __calculate_expected_moves(self, partition):
		LOG_DIVISOR = math.log(len(partition))
		if LOG_DIVISOR == 0:
			return len(self.__possible_words) + 1

		CORRECT_COMPARISON = 'G'*5
		expected_moves = 0

		# Remove the correct comparison because its expected move is 0
		if CORRECT_COMPARISON in partition:
			partition.pop(CORRECT_COMPARISON)

		for comparison in partition:
			frac_of_total_space = partition[comparison] / len(self.__possible_words)
			comparison_exp_moves = (math.log(partition[comparison]) / LOG_DIVISOR) + 1
			expected_moves += frac_of_total_space * comparison_exp_moves

		return expected_moves

	def __partition_score(self, word):
		partition = self.__aggregate_comparisons(word)
		return self.__calculate_expected_moves(partition)

	def __calculate_word_scores(self):
		if len(self.__possible_words) == 0:
			return {}
			
		# Determine the best guess word
		# This is done by choosing the word whose possible results partition the remaining possible
		# word list into the most equal chunks.
		guess_word_scores = {}
		if self.__HARD:
			guessable_words = self.__possible_words
		else:
			guessable_words = Wordle.__dictionary

		for guess_word in guessable_words:
			guess_word_scores[guess_word] = self.__partition_score(guess_word)

		return guess_word_scores

	def __parse_response_green_yellow(self, entered_word, response):
		new_letter_counts = {}

		for pos, char in enumerate(response):
			if char == '_':
				# We'll deal with these later
				continue

			elif char == char.upper():
				letter = char.upper()

				# Set position to be only this green letter
				self.__possible_letters[pos] = [letter]

				# Add letter to must-haves
				if letter not in new_letter_counts:
					new_letter_counts[letter] = 1
				else:
					new_letter_counts[letter] += 1

			elif char == char.lower():
				letter = char.upper()

				# Add letter to must-haves
				if letter not in new_letter_counts:
					new_letter_counts[letter] = 1
				else:
					new_letter_counts[letter] += 1

				# Remove letter from this position
				if letter in self.__possible_letters[pos]:
					self.__possible_letters[pos].remove(letter)

		# Check to see if we need more of a given letter than we previously thought
		for letter in new_letter_counts:
			if letter not in self.__letter_counts:
				self.__letter_counts[letter] = [new_letter_counts[letter], None]
			elif new_letter_counts[letter] > self.__letter_counts[letter][0]:
				self.__letter_counts[letter][0] = new_letter_counts[letter]

	def __parse_response_gray(self, entered_word, response):
		for pos, char in enumerate(response):
			if char == '_':
				letter = entered_word[pos]

				# Remove letter from this position
				if letter in self.__possible_letters[pos]:
					self.__possible_letters[pos].remove(letter)

				# Set the maximum number of letter occurrences if this letter is a must-have
				if letter in self.__letter_counts:
					self.__letter_counts[letter][1] = self.__letter_counts[letter][0]

				# Remove letter from other positions if it is not green or yellow elsewhere
				else:
					for pos_list in self.__possible_letters:
						if letter in pos_list:
							pos_list.remove(letter)

	def __pare_word_list(self):
		new_possible_words = []
		for word in self.__possible_words:

			# Check that all must-haves are in word
			for letter in self.__letter_counts:
				if  (
						(self.__letter_counts[letter][0] is not None) and
						(word.count(letter) < self.__letter_counts[letter][0])
					) or (
						(self.__letter_counts[letter][1] is not None) and
				   		(word.count(letter) > self.__letter_counts[letter][1])
				   	):
				   break
			else:
				# Check that each position only has allowable letters for that position
				for pos, letter in enumerate(word):
					if letter not in self.__possible_letters[pos]:
						break
				else:
					new_possible_words.append(word)

		self.__possible_words = new_possible_words

	def guess(self, entered_word, response):
		entered_word = entered_word.upper()
		if len(entered_word) != 5:
			raise "Entered word is not 5 letters!"
		for char in entered_word:
			if char == char.lower():
				raise "Entered word contains invalid characters!"
		if len(response) != 5:
			raise "Response is not 5 characters!"
		for pos, char in enumerate(response):
			if (char != '_') and (char.upper() != entered_word[pos]):
				raise "Response does not match entered word!"

		# First, look for green and yellow letters
		self.__parse_response_green_yellow(entered_word, response)

		# Next, look for gray letters
		# This is done in a separate iteration because if a letter is repeated with
		# the first instance gray and the second yellow or green, we need to know
		# about that later instance before we can decide what to do with the gray
		# one.
		self.__parse_response_gray(entered_word, response)

		# Pare down word list from parsed response
		self.__pare_word_list()

		# Calculate new best word scores
		self.__guess_word_scores = self.__calculate_word_scores()
		
	def possible_words(self):
		return self.__possible_words

	def best_guesses(self, with_scores=False):
		sorted_word_scores = sorted(self.__guess_word_scores.items(), key=lambda x: x[1])

		if with_scores:
			return sorted_word_scores
		else:
			return list(map(lambda x: x[0], sorted_word_scores))
